# Dépôt mime
Author      Tom De Rudder  
Version     0.0.1
___
> "_mime_" est un jeu mobile développé avec le framework ionic. Il propose un mime au joureur(s) parmit plusieur catgories (métier, animal, etc).

## Architecture
```bash
    mime
    ├───.sourcemaps
    ├───platforms
    │   └───android
    ├───plugins
    │   ├───cordova-plugin-device
    │   ├───cordova-plugin-ionic-keyboard
    │   ├───cordova-plugin-ionic-webview
    │   ├───cordova-plugin-splashscreen
    │   └───cordova-plugin-whitelist
    ├───resources
    │   ├───android
    │   └───ios
    ├───src
    │   ├───app
    │   ├───assets
    │   ├───pages
    │   └───theme
    └───www
        ├───assets
        └───build
	└── README.md
```

## Pré requis
- Node.Js
- Cordova
- Android SDK

## Installation
```bash
    git clone "https://gitlab.com/tomderudderetna/mime.git" mime
    cd ./mime
```

## Liens utiles
 - [documentation de ionic](https://ionicframework.com/docs/)
 - [install the Android SDK](https://www.androidcentral.com/installing-android-sdk-windows-mac-and-linux-tutorial)
 - [documentation de sdkmanager](https://developer.android.com/studio/command-line/sdkmanager)


## Logo
![logo](resources/android/icon/drawable-xxhdpi-icon.png)